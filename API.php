<?php
require_once "DB.php";

class API {
    public function init() {
        header('Content-Type: application/JSON');
        header('Access-Control-Allow-Origin: *');
        $metodo = $_SERVER['REQUEST_METHOD'];
        
        $this->db = new DB();

        switch($metodo) {
            case 'GET':
                if (isset($_GET["usuario"])) {
                    $this->usuario();
                }
                if (isset($_GET["verificarusuario"])) {
                    $this->verificarUsuario();
                }
                if (isset($_GET["juego"])) {
                    $this->juego();
                }
				if (isset($_GET["listajuego"])) {
					$this->listaJuego();
				}
				if (isset($_GET["obtenerejecutar"])) {
					$this->obtenerEjecutar();
				}
				if (isset($_GET["obtenerjuegosusuario"])) {
					$this->obtenerJuegosUsuario();
				}
				if (isset($_GET["todosusuarios"])) {
					$this->todosUsuarios();
				}
				if (isset($_GET["todosjuegos"])) {
					$this->todosJuegos();
				}
				if (isset($_GET["ultimosjuegos"])) {
					$this->ultimosJuegos();
				}
                if (isset($_GET["prueba"])) {
                    $this->prueba();
                }
                break;
            case 'POST':
				if (isset($_GET["agregarjuegousuario"])){
					$this->agregarJuegoUsuario();
				}
				if (isset($_GET["juegoagregar"])) {
					$this->juegoAgregar();
				}
                if (isset($_GET["registrar"])) {
                    $this->registrar();
                }
				if (isset($_GET["modificarusuario"])) {
					$this->modificarUsuario();
				}
				if (isset($_GET["verificaremail"])) {
					$this->verificarEmail();
                }
                if (isset($_GET["borrarjuegousuario"])) {
                    $this->borrarJuegoUsuario();
                }
                break;
            default:
                echo "Metodo no soportado";
                break;
        }
    }

    function verificarUsuario() {
		$response = array();

        if (isset($_GET["dato"])) {
            $response = $this->db->verificarUsuario($_GET["dato"]);

            $response["estado"] = true;
        } else {
			$response["verificado"] = false;
        }

		$response["estado"] = true;

		echo json_encode($response);
    }

    function usuario() {
		$response = array();

        if (isset($_GET["dato"])) {
            $response = $this->db->getUsuario($_GET["dato"]);
        }

		$response["estado"] = true;

		echo json_encode($response);
    }

    function juego() {
        if (isset($_GET["dato"])) {
            $response = $this->db->getJuego($_GET["dato"]);
        }

		$response["estado"] = true;

		echo json_encode($response);
    }

    function registrar() {
		$response = array();

        if (isset($_GET["dato"])) {
			$response["useruso"] = $this->db->verificarUser($_GET["dato"]);

			$response["emailuso"] = $this->db->verificarEmail($_GET["dato"]);

			$response["agregado"] = false;

			if (!$response["useruso"] && !$response["emailuso"])
            	$response["agregado"] = $this->db->agregarUsuario($_GET["dato"]);
        }

		$response["estado"] = true;

		echo json_encode($response);
    }

	function listaJuego() {
		$response["juegos"] = $this->db->listaJuegos();

		$response["estado"] = true;

		echo json_encode($response);
	}

	function obtenerEjecutar() {
		$response = array();

		$response = $this->db->juegoEjecutar($_GET["dato"]);

		$response["estado"] = true;

		echo json_encode($response);
	}

	function obtenerJuegosUsuario() {
		$response = array();

		$response["juegos"] = $this->db->obtenerJuegosUsuario($_GET["dato"]);
		$response["estado"] = true;

		echo json_encode($response);
	}

	function todosUsuarios() {
		$response = array();

		$response = $this->db->todosUsuarios($_GET["dato"]);
		$response["estado"] = true;

		echo json_encode($response);
	}

	function todosJuegos() {
		$response = array();

		$response = $this->db->todosJuegos($_GET["dato"]);
		$response["estado"] = true;

		echo json_encode($response);
	}

	function ultimosJuegos() {
		$response = array();

		$response["juegos"] = $this->db->ultimosJuegos();
		$response["estado"] = true;

		echo json_encode($response);
	}

	function agregarJuegoUsuario() {
		$response = array();

		$response = $this->db->agregarJuegoUsuario($_GET["dato"]);
		$response["estado"] = true;

		echo json_encode($response);
	}

	function juegoAgregar() {
		$response = array();

		$response["agregado"] = $this->db->juegoAgregar();
		$response["estado"] = true;

		echo json_encode($response);
	}

	function modificarUsuario() {
		$response = array();

		$response = $this->db->modificarUsuario($_GET["dato"]);
		$response["estado"] = true;

		echo json_encode($response);
	}

	function verificarEmail() {
		$response = array();

		$response["verificado"] = $this->db->verificarEmailCodigo($_GET["dato"]);
		$response["estado"] = true;

		echo json_encode($response);
    }

    function borrarJuegoUsuario() {
        $response = array();

        $response["borrado"] = $this->db->borrarJuegoUsuario($_GET["dato"]);
        $response["estado"] = true;

        echo json_encode($response);
    }

    function prueba() {
        $response = array();

        $response = $this->db->prueba($_GET["dato"]);
        $response["estado"] = true;
        
        echo json_encode($response);
    }
}
