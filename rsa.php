<?php
const KEY_BITS = 4096;
const ENCRYPT_BLOCK_SIZE = KEY_BITS / 8 - 11;
const DECRYPT_BLOCK_SIZE = KEY_BITS / 8;

function encriptar_server_cliente($texto) {
    $key_private_server = openssl_get_privatekey("file://clave/private_server.pem");
    $key_public_cliente = openssl_get_publickey("file://clave/public_client.pem");

    $primer_encriptacion = openssl_private_encrypt($texto, $encriptado, $key_private_server);

    if ($primer_encriptacion) {
        $split_data = str_split($encriptado, ENCRYPT_BLOCK_SIZE);

        $final = "";

        foreach ($split_data as $chunk) {
            $parcial = "";

            $segunda_encriptacion = openssl_public_encrypt($chunk, $parcial, $key_public_cliente);

            if ($segunda_encriptacion == false) {
                echo "Error en la segunda encriptacion";
                return "";
            }

            $final .= $parcial;
        }

        return base64_encode($final);

    } else {
        echo "Error en la primera encriptacion";
    }
}

function desencriptar_cliente_server($encriptado) {
	while ($msg = openssl_error_string()) {};
    $key_private_server = openssl_get_privatekey("file://clave/private_server.pem");
	while ($msg = openssl_error_string()){
    	echo "OpenSSL error when doing key_private_server:" . $msg . "<br />\n";
	}

    $key_public_cliente = openssl_get_publickey("file://clave/client.csr");

	while ($msg = openssl_error_string()){
    	echo "OpenSSL error when doing key_public_cliente:" . $msg . "<br />\n";
	}

	$data_split = str_split(base64_decode($encriptado), DECRYPT_BLOCK_SIZE);

	$desencriptado_server = "";

	foreach ($data_split as $chunk) {
		$parcial = "";

        echo "El chunk es: ".$chunk;
        echo "El tamaño del chunk es: ".strlen($chunk);
		$ok = openssl_private_decrypt($chunk, $parcial, $key_private_server);
        echo "El desencriptado parcial es: ".$parcial;
		while ($msg = openssl_error_string()){
    		echo "OpenSSL error when doing Decrypt private:" . $msg . "<br />\n";
		}

		if ($ok === false) {
			echo openssl_error_string();
			echo "Error en la primera desencriptacion";
			return "";
		}

		$desencriptado_server .= $parcial;
	}

	$texto = "";

	$data_split_2 = str_split($desencriptado_server, DECRYPT_BLOCK_SIZE);

	foreach ($data_split_2 as $chunk) {
		$parcial = "";

		$ok = openssl_public_decrypt($chunk, $parcial, $key_public_cliente, OPENSSL_PKCS1_PADDING);

		if ($ok === false) {
			echo "Error en la segunda desencriptacion";
			return "";
		}

		$texto .= $parcial;
	}

	return $texto;
}
