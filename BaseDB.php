<?php

class BaseDB {
	protected $mysqli;
    protected const HOST = "localhost";
    protected const USER = "bax";
    protected const PASS = "40735180";
    protected const DB = "bax";
    protected const USUARIOS_TABLE = "usuarios_bax";
    protected const JUEGOS_TABLE = "juegos_bax";

	public function __construct() {
        try {
            $this->mysqli = new mysqli(self::HOST, self::USER, self::PASS, self::DB);
        }catch (mysqli_sql_exception $e) {
            http_response_code(500);
            exit;
        }
    }
}
