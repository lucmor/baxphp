CREATE TABLE `juegos_bax` (
    `id` int(5) NOT NULL AUTO_INCREMENT,
    `titulo` text NOT NULL,
    `desarrollador` text NOT NULL,
    `descripcion` text NOT NULL,
    `genero` text NOT NULL,
    `exe` text NOT NULL,
    `ultversion` text NOT NULL,
    `imagen` text NOT NULL,
    `args` text NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `usuarios_bax` (
    `id` int(5) NOT NULL AUTO_INCREMENT,
    `nombre` text NOT NULL,
    `apellido` text NOT NULL,
    `usuario` text NOT NULL,
    `password` text NOT NULL,
    `email` text NOT NULL,
    `admin` tinyint(1) NOT NULL,
    `juegos` text NOT NULL,
    `emailNuevo` text,
    `codigo` int(6),
    `verificado` tinyint(1) NOT NULL,
    `imagen` text NOT NULL,
    `amigos` text NOT NULL,
    PRIMARY KEY (`id`)
);
