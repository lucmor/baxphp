<?php
require "BaseDB.php";
require "rsa.php";

/**
 * Autor: nahuel
 */

class DB extends BaseDB{
	public function __construct() {
		parent::__construct();
	}

    public function getUsuario($dato) {
		$json_dato = json_decode(base64_decode($dato), true);

		return $this->getUsuario_($json_dato["usuario"], $json_dato["dato"]);
	}

	private function getUsuario_($usuario, $dato) {

        if ($dato == "todos") {
            $sql = "SELECT nombre, apellido, usuario, email FROM ".self::USUARIOS_TABLE." WHERE usuario = ?";
        } else {
            $sql = "SELECT ".$dato." FROM ".self::USUARIOS_TABLE." WHERE usuario = ?";
        }

        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('s', $usuario);
        $stmt->execute();

        if ($dato == "todos") {
            $stmt->bind_result($nombre, $apellido, $usuario, $email);
        } else {
            $stmt->bind_result($dato_db);
        }

        while ($stmt->fetch()){
            if ($dato == "todos"){
                $datos["nombre"] = $nombre;
                $datos["apellido"] = $apellido;
                $datos["usuario"] = $usuario;
                $datos["email"] = $email;
            } else {
                $datos[$dato] = $dato_db;
            }
        }

        $stmt->close();

        return $datos;
    }

    public function getJuego($datos) {
        $json_data = json_decode(base64_decode($datos), true);

        $sql = "SELECT id, titulo, descripcion, genero, exe, ultversion from ".self::JUEGOS_TABLE." WHERE id = ?";

		$stmt = $this->mysqli->prepare($sql);

		$stmt->bind_param('s', $json_data["id"]);

		$stmt->execute();

		$stmt->bind_result($id, $titulo, $descripcion, $genero, $exe, $ultversion);

		$fila = array();

		while ($stmt->fetch()) {
			$fila["id"] = $id;
			$fila["titulo"] = $titulo;
			$fila["descripcion"] = $descripcion;
			$fila["genero"] = $genero;
			$fila["exe"] = $exe;
			$fila["ultversion"] = $ultversion;
		}

        $stmt->close();

        $juegos = json_decode($this->getUsuario_($json_data["user"], "juegos")["juegos"], true);

        $fila["path"] = $juegos[intval($json_data["id"])]["path"];

		return $fila;
    }

    public function agregarUsuario($datos) {
        $codigo = rand(100000, 999999);

        $sql = "INSERT INTO ".self::USUARIOS_TABLE." (nombre, apellido, usuario, password, email, admin, juegos, codigo) VALUES (?, ?, ?, ?, ?, 0, \"{}\", ?)";

        $json_data = json_decode(base64_decode($datos), true);

        $stmt = $this->mysqli->prepare($sql);

        $stmt->bind_param('sssssi', $json_data["nombre"], $json_data["apellido"], $json_data["usuario"], password_hash($json_data["password"], PASSWORD_DEFAULT), $json_data["email"], $codigo);

        $stmt->execute();

        if ($stmt->affected_rows == 1){
            $html_plantilla = file_get_contents('plantillas/mail.html');

            $html_plantilla = str_replace("{{ user }}", $json_data["usuario"], $html_plantilla);
            $html_plantilla = str_replace("{{ accion }}", "la verificacion de email", $html_plantilla);
            $html_plantilla = str_replace("{{ codigo }}", $codigo, $html_plantilla);

            $headers = "Content-Type: text/html; charset=utf-8\r\n";

            mail($json_data["email"], "BAX: Verificacion de correo", $html_plantilla, $headers);
            return true;
        } else {
            return false;
        }
    }

    public function verificarUsuario($dato) {
        $json_dato = json_decode(base64_decode($dato), true);

        $user_data = $this->getUsuario_($json_dato["usuario"], "password");
        $datos["verificado"] = password_verify($json_dato["password"], $user_data["password"]);

        $user_data = $this->getUsuario_($json_dato["usuario"], "admin");
        $datos["admin"] = (bool) $user_data["admin"];

        $user_data = $this->getUsuario_($json_data["usuario"], "verificado");
        $datos["emailverificado"] = (bool) $user_data["verificado"];

        return $datos;
    }

	public function listaJuegos() {
		$sql = "SELECT titulo, id FROM ".self::JUEGOS_TABLE;

		$stmt = $this->mysqli->prepare($sql);
		$stmt->execute();

		$stmt->bind_result($titulo, $id);

		$datos = array();
		$fila = array();

		while ($stmt->fetch()) {
			$fila["titulo"] = $titulo;
			$fila["id"] = $id;

			array_push($datos, $fila);
		}

		$stmt->close();

		return $datos;
	}

	public function verificarUser($dato) {
		$json_dato = json_decode(base64_decode($dato), true);

		$sql = "SELECT usuario FROM ".self::USUARIOS_TABLE." WHERE usuario = ?";

		$stmt = $this->mysqli->prepare($sql);

		$stmt->bind_param('s', $json_dato["usuario"]);

        $stmt->execute();

		$stmt->store_result();

		return $stmt->num_rows != 0 && strlen($json_dato["usuario"]) <= 15;
	}

	public function verificarEmail($dato) {
		$json_dato = json_decode(base64_decode($dato), true);

		$sql = "SELECT email FROM ".self::USUARIOS_TABLE." WHERE email = ?";

		$stmt = $this->mysqli->prepare($sql);

		$stmt->bind_param('s', $json_dato["email"]);

		$stmt->execute();

		$stmt->store_result();

		return $stmt->num_rows != 0 && filter_var($json_dato["email"], FILTER_VALIDATE_EMAIL);
	}

	public function juegoEjecutar($dato) {
		$json_dato = json_decode(base64_decode($dato), true);

		$juegos_usuario = json_decode($this->getUsuario_($json_dato["user"], "juegos")["juegos"], true);

		$sql = "SELECT exe, args FROM ".self::JUEGOS_TABLE." WHERE id = ?";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param('i', $json_dato["id"]);

		$stmt->execute();

		$stmt->bind_result($exe, $args);

		$fila = array();

		while ($stmt->fetch()) {
			$fila["path"] = $juegos_usuario[$json_dato["id"]]["path"].$exe;
			$fila["args"] = $args;
		}

		return $fila;
	}

	public function obtenerJuegosUsuario($dato) {
		$json_dato = json_decode(base64_decode($dato), true);

		return json_decode($this->getUsuario_($json_dato["usuario"], "juegos")["juegos"]);
	}

	public function todosUsuarios($dato) {
		$json_dato = json_decode(base64_decode($dato), true);

		$admin_usuario = $this->getUsuario_($json_dato["usuario"], "admin")["admin"];

		$datos = array();

		if (intval($admin_usuario) == 1) {
                        $datos["esAdmin"] = true;

			$sql = "SELECT id, nombre, apellido, usuario, email, admin FROM ".self::USUARIOS_TABLE;

			$stmt = $this->mysqli->prepare($sql);

			$stmt->execute();

			$stmt->bind_result($id, $nombre, $apellido, $usuario, $email, $admin);

			$datos["data_users"] = array();

			while ($stmt->fetch()) {
				$datos["data_users"][(string) $id] = array();
				$datos["data_users"][(string) $id]["nombre"] = $nombre;
				$datos["data_users"][(string) $id]["apellido"] = $apellido;
				$datos["data_users"][(string) $id]["usuario"] = $usuario;
				$datos["data_users"][(string) $id]["email"] = $email;
				if ($admin == 1) {
					$datos["data_users"][(string) $id]["admin"] = "si";
				} else {
					$datos["data_users"][(string) $id]["admin"] = "no";
				}
			}
		} else {
			$datos["esAdmin"] = false;
		}

		return $datos;
	}

	public function todosJuegos($dato) {
		$json_dato = json_decode(base64_decode($dato), true);

		$admin_usuario = $this->getUsuario_($json_dato["usuario"], "admin")["admin"];

		$datos = array();

		if (intval($admin_usuario) == 1) {
                        $datos["esAdmin"] = true;
			$sql = "SELECT id, titulo, desarrollador, genero, exe, ultversion FROM ".self::JUEGOS_TABLE;

			$stmt = $this->mysqli->prepare($sql);

			$stmt->execute();

			$stmt->bind_result($id, $titulo, $desarrollador, $genero, $exe, $ultversion);

			$datos["data_juegos"] = array();

			while ($stmt->fetch()) {
				$datos["data_juegos"][$id] = array();
				$datos["data_juegos"][$id]["titulo"] = $titulo;
				$datos["data_juegos"][$id]["desarrollador"] = $desarrollador;
				$datos["data_juegos"][$id]["genero"] = $genero;
				$datos["data_juegos"][$id]["exe"] = $exe;
				$datos["data_juegos"][$id]["ultversion"] = $ultversion;
			}
		} else {
			$datos["esAdmin"] = false;
		}

		return $datos;
	}

	public function ultimosJuegos() {
		$sql = "SELECT titulo, imagen FROM ".self::JUEGOS_TABLE." ORDER BY id DESC LIMIT 3";

		$stmt = $this->mysqli->prepare($sql);

		$stmt->execute();

		$stmt->bind_result($titulo, $imagen);

		$datos = array();
		$fila = array();

        while ($stmt->fetch()) {
            $fila["titulo"] = $titulo;
			$fila["imagen"] = $imagen;

			array_push($datos, $fila);
		}

		return $datos;
	}

    public function agregarJuegoUsuario($dato) {
        $devolver = array();

		$json_dato = json_decode(base64_decode($dato), true);

		$juegos_usuario = json_decode($this->getUsuario_($json_dato["usuario"], "juegos")["juegos"], true);

		if (!isset($juegos_usuario[$json_dato["id"]])) {
			$sql = "SELECT titulo, exe FROM ".self::JUEGOS_TABLE." WHERE id = ?";

			$stmt = $this->mysqli->prepare($sql);
			$stmt->bind_param('i', $json_dato["id"]);

			$stmt->execute();

			$stmt->bind_result($titulo, $exe);

            $stmt->fetch();

            $explode_dir = explode("\\", $json_dato["dir"]);
            $nombre_archivo = $explode_dir[count($explode_dir) - 1];

            if ($exe == $nombre_archivo) {
    			$juegos_usuario[$json_dato["id"]] = array();
                $juegos_usuario[$json_dato["id"]]["id"] = $json_dato["id"];
		    	$juegos_usuario[$json_dato["id"]]["titulo"] = $titulo;
			    $juegos_usuario[$json_dato["id"]]["path"] = str_replace($nombre_archivo, "", $json_dato["dir"]);

    			$sql = "UPDATE ".self::USUARIOS_TABLE." SET juegos = ? WHERE usuario = ?";

                $stmt->close();

	    		$stmt_2 = $this->mysqli->prepare($sql);

    			$stmt_2->bind_param('ss', json_encode($juegos_usuario), $json_dato["usuario"]);

    			$stmt_2->execute();

                $devolver["agregado"] = $stmt_2->affected_rows != 0;
            } else {
                $devolver["exeIncorrecto"] = true;
            }
        } else {
            $devolver["yaAgregado"] = true;
        }

        return $devolver;
	}

	public function juegoAgregar() {
		$carpeta_images = "images/";
		$nombre_archivo = strtolower($_FILES["caratulaFile"]["name"]);

		if (move_uploaded_file($_FILES["caratulaFile"]["tmp_name"], $carpeta_images.base64_encode($nombre_archivo))){
			$sql = "INSERT INTO ".self::JUEGOS_TABLE." (titulo, desarrollador, descripcion, genero, exe, ultversion, imagen, args) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

			$stmt = $this->mysqli->prepare($sql);
			$stmt->bind_param('ssssssss', $_POST["titulo"], $_POST["desarrollador"], $_POST["descripcion"], $_POST["genero"], $_POST["exe"], $_POST["ultversion"], $nombre_archivo, $_POST["attrib"]);

			$stmt->execute();

			return $stmt->affected_rows != 0;
		} else {
			return false;
		}
	}

	public function modificarUsuario($dato) {
		$json_dato = json_decode(base64_decode($dato), true);

		$datos = array();

		$sql = "UPDATE ".self::USUARIOS_TABLE." SET ".$json_dato["dato"]." = ? WHERE usuario = ?";

		if ($json_dato["dato"] == "password"){
			$pass = $this->getUsuario_($json_dato["user"], "password")["password"];
			if (password_verify($json_dato["datoViejo"], $pass)) {
				if ($json_dato["datoNuevo"] == $json_dato["datoNuevoConfirm"]){
					$stmt = $this->mysqli->prepare($sql);

					$stmt->bind_param("ss",password_hash($json_dato["datoNuevo"], PASSWORD_DEFAULT), $json_dato["user"]);

					$stmt->execute();

					$datos["modificado"] = $stmt->affected_rows != 0;
				} else {
					$datos["modificado"] = false;
					$datos["passNuevaMal"] = true;
				}
			} else {
				$datos["modificado"] = false;
				$datos["passviejaMal"] = true;
			}
		} elseif ($json_dato["dato"] == "email") {
			$codigo = rand(100000, 999999);

			$sql = "UPDATE ".self::USUARIOS_TABLE." SET emailNuevo = ?, codigo = ? WHERE usuario = ?";

			$stmt = $this->mysqli->prepare($sql);

			$stmt->bind_param("sis", $json_dato["datoNuevo"], $codigo, $json_dato["user"]);

			$stmt->execute();

			$datos["modificado"] = $stmt->affected_rows != 0;

			$html_plantilla = file_get_contents('plantillas/mail.html');
            $html_plantilla = str_replace("{{ user }}", $json_dato["user"], $html_plantilla);
            $html_plantilla = str_replace("{{ accion }}", "el cambio de correo", $html_plantilla);
            $html_plantilla = str_replace("{{ codigo }}", $codigo, $html_plantilla);

            $headers = "Content-Type: text/html; charset=utf-8\r\n";

            mail($json_dato["datoNuevo"], "BAX: Cambio de correo", $html_plantilla, $headers);
        } elseif($json_dato["dato"] == "dir") {
            $juegos = json_decode($this->getUsuario_($json_dato["user"], "juegos")["juegos"], true);

            $explode_dir = explode("\\", $json_dato["nuevoPath"]);
            $nombre = $explode_dir[count($explode_dir) - 1];
            
            $sql = "SELECT exe FROM ".self::JUEGOS_TABLE." WHERE id = ?";

            $stmt = $this->mysqli->prepare($sql);

            $stmt->bind_param("s", $json_dato["idJuego"]);

            $stmt->execute();

            $stmt->bind_result($exe);

            $stmt->fetch();
            
            if ($nombre == $exe) {
                $juegos[$json_dato["idJuego"]]["path"] = str_replace($nombre, "", $json_dato["nuevoPath"]);

                $stmt->close();

                $sql = "UPDATE ".self::USUARIOS_TABLE." SET juegos = ? WHERE usuario = ?";

                $stmt_2 = $this->mysqli->prepare($sql);

                echo $this->mysqli->error;

                $stmt_2->bind_param("ss", json_encode($juegos), $json_dato["user"]);

                $stmt_2->execute();

                $datos["dirModificado"] = $stmt_2->affected_rows != 0;
            } else {
                $datos["exeIncorrecto"] = true;
            }
        } else {
			$stmt->bind_param("ss", $json_dato["datoNuevo"], $json_dato["user"]);

			$stmt->execute();

			$datos["modificado"] = $stmt->affected_rows != 0;
		}

		return $datos;
	}

	public function verificarEmailCodigo($dato) {
	    $json_dato = json_decode(base64_decode($dato), true);	

		$sql = "SELECT codigo, emailNuevo, verificado FROM ".self::USUARIOS_TABLE." WHERE usuario = ?";

		$stmt = $this->mysqli->prepare($sql);
		$stmt->bind_param("s", $json_dato["user"]);

		$stmt->execute();

		$stmt->bind_result($codigo, $emailNuevo, $verificado);

		$stmt->fetch();

        $stmt->close();
        if (intval($json_dato["codigo"]) == intval($codigo)) {
            if ($verificado == 0) {
                $sql = "UPDATE ".self::USUARIOS_TABLE." SET codigo = null, verificado = 1 WHERE usuario = ?";

                $stmt = $this->mysqli->prepare($sql);

                $stmt->bind_param("s", $json_dato["user"]);

                $stmt->execute();
            } else {
    			$sql = "UPDATE ".self::USUARIOS_TABLE." SET email = ?, emailNuevo = null, codigo = null WHERE usuario = ?";

	    		$stmt = $this->mysqli->prepare($sql);

		    	$stmt->bind_param("ss", $emailNuevo, $json_dato["user"]);
    
	    		$stmt->execute();

            }

			return $stmt->affected_rows != 0;
		} else {
			return false;
		}
    }

    public function borrarJuegoUsuario($dato) {
        $json_dato = json_decode(base64_decode($dato), true);        
        $juegos = json_decode($this->getUsuario_($json_dato["user"], "juegos")["juegos"], true);

        unset($juegos[$json_dato["id"]]);

        $sql = "UPDATE ".self::USUARIOS_TABLE." SET juegos = ? WHERE usuario = ?";

        $stmt = $this->mysqli->prepare($sql);
        
        if (json_encode($juegos) == "[]") {
            $juegos_encode = "{}";
            $stmt->bind_param("ss", $juegos_encode, $json_dato["user"]);
        } else {
            $stmt->bind_param("ss", json_encode($juegos), $json_dato["user"]);
        }

        $stmt->execute();

        return $stmt->affected_rows != 0;
    }

	public function prueba($dato) {
		$json_dato = json_decode(base64_decode($dato), true);
		$devolver = array();

		$devolver["datoEncriptado"] = encriptar_server_cliente($json_dato["datoAEncriptar"]);
		$devolver["datoDesencriptado"] = desencriptar_cliente_server($json_dato["datoEncriptado"]);

		return $devolver;
	}

	private function desencriptar_data($data) {
		foreach ($data as $key => $val) {
			$data[$key] = desencriptar_cliente_server($val);
		}
	}
}
